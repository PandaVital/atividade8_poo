package br.ucsal.bes20182.poo.atividade8;

import java.util.Date;

public class Caminh�o extends Veiculo {

	private Integer quantidadeDeEixos;

	private Enum<TipoDeCargaEnum> tipoDeCarga;

	public Caminh�o(String placa, Date anoDeFabricacao, Integer valor, Integer quantidadeDeEixos,
			Enum<TipoDeCargaEnum> tipoDeCarga) {
		super(placa, anoDeFabricacao, valor);
		this.quantidadeDeEixos = quantidadeDeEixos;
		this.tipoDeCarga = tipoDeCarga;
	}

	public Integer getQuantidadeDeEixos() {
		return quantidadeDeEixos;
	}

	public void setQuantidadeDeEixos(Integer quantidadeDeEixos) {
		this.quantidadeDeEixos = quantidadeDeEixos;
	}

	public Enum<TipoDeCargaEnum> getTipoDeCarga() {
		return tipoDeCarga;
	}

	public void setTipoDeCarga(Enum<TipoDeCargaEnum> tipoDeCarga) {
		this.tipoDeCarga = tipoDeCarga;
	}

}
