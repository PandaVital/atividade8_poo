package br.ucsal.bes20182.poo.atividade8;

import java.util.Date;

public class Motos extends Veiculo {

	private Enum<TipoMotosEnum> tipoMotor;

	public Motos(String placa, Date anoDeFabricacao, Integer valor, Enum<TipoMotosEnum> tipoMotor) {
		super(placa, anoDeFabricacao, valor);
		this.tipoMotor = tipoMotor;
	}

	public Enum<TipoMotosEnum> getTipoMotor() {
		return tipoMotor;
	}

	public void setTipoMotor(Enum<TipoMotosEnum> tipoMotor) {
		this.tipoMotor = tipoMotor;
	}

}
