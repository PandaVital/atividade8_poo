package br.ucsal.bes20182.poo.atividade8;

import java.util.Date;

public class Carro extends Veiculo {

	private Integer quantidadeDePortas;

	public Carro(String placa, Date anoDeFabricacao, Integer valor, Integer quantidadeDePortas) {
		super(placa, anoDeFabricacao, valor);
		this.quantidadeDePortas = quantidadeDePortas;
	}

	public Integer getQuantidadeDePortas() {
		return quantidadeDePortas;
	}

	public void setQuantidadeDePortas(Integer quantidadeDePortas) {
		this.quantidadeDePortas = quantidadeDePortas;
	}

}
